
Install the packages first
```
npm run install
```


to run the program against real Arduino:

```
npm run prod
```

to test it against mock data:
```
npm run start
```