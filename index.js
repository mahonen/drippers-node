const readline = require("readline");
const request = require("request");
//const
const HEATING = "Pot is heating";
const COOLING = "Pot is cooling";
const STALE = "Pot is stale";
const URL = "http://51.68.128.220:8080/api/ready";
//config
const TARGET_TEMP = 38.5;
const ROOM_TEMP = 22.0;
const TIMER = 1000 * 60 * 5; // 5 Minutes - Time after coffee is ready to enter pseudo-sleep mode
const ROOM_TEMP_DEVIATION = 5.0;
const MAX_DEVIATION = 5; // Acceptable amount of deviation between min and max of samples
const SAMPLE_SIZE_TOTAL = 10;
const SAMPLE_SIZE = 4;
const STATE_LENGTH = 4;
const STALENESS = 0.5; // Acceptable amount of deviation between sample points to be considered stale
//initialize variables
let measurements = [];
let states = [];
let isFresh = true;

const initialize = () => {
  // fill with dummy data / room temp is perfect
  measurements.length = SAMPLE_SIZE_TOTAL;
  measurements.fill(ROOM_TEMP, 0, SAMPLE_SIZE_TOTAL);
};

// helper funtions
const parser = text => {
  const result = parseFloat(text);
  return result;
};

const pushState = value => {
  if (value) {
    if (states.length > STATE_LENGTH) {
      states.shift();
    }
    states.push(value);
    return true;
  }
  return false;
};
const average = array => {
  let sum,
    avg = 0;
  sum = array.reduce((a, b) => a + b);
  return (avg = sum / array.length);
};

// read the coffee temp and make assumptions of its 'readyness'
const coffeeState = temps => {
  const min = Math.min(...temps);
  const max = Math.max(...temps);
  const deviation = max - min;
  let acceptable = false;
  if (deviation < MAX_DEVIATION) {
    acceptable = true;
  }

  if (acceptable) {
    const oldest = temps.slice(0, SAMPLE_SIZE);
    const newest = temps.slice(-4);

    const oldestAvg = average(oldest);
    const newestAvg = average(newest);

    const isStale = (old, newVal) => {
      const value = Math.abs(old - newVal);
      if (value < STALENESS) {
        return true;
      }
      return false;
    };
    if (oldestAvg === newestAvg || isStale(oldestAvg, newestAvg)) {
      return STALE;
    }
    if (oldestAvg > newestAvg) {
      return COOLING;
    }
    return HEATING;
  }
  return false;
};

// check if we can tweet about the coffee - only once every TIMER's value
const willTweet = () => {
  const coffeeTemp = average(measurements);
  if (coffeeTemp >= TARGET_TEMP && isFresh) {
    isFresh = false;
    setTimeout(() => {
      isFresh = true;
    }, TIMER);

    return true;
  }
  return false;
};

// init and start reading stdout of arduino

initialize();
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

// attempt to deduce the coffees readyness from arduinos heat readings

rl.on("line", line => {
  try {
    measurements.shift();
    measurements.push(parser(line));
    console.log(measurements);
    const cState = coffeeState(measurements);
    pushState(cState);
    console.log(states);
    const tweet = willTweet();
    if (tweet) {
      request({
        method: "POST",
        uri: URL
      });
      console.log("tweeted");
    }
  } catch (err) {
    console.log(err);
  }
});
